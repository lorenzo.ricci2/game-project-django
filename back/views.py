from django.shortcuts import render
from django.views import generic
from .models import Post


class ChatView(generic.ListView):
    model = Post
    template_name = 'chat.html'
