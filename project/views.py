from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views import generic
from back.models import Post, Profile, Reply
from django.urls import reverse_lazy
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView


def home_page(request):
    return render(request, 'home.html')

def login(request):
    return render(request, 'login.html')

def register(request):
    if request.method=="POST":
        username = request.POST['username']
        email = request.POST['email']
        first_name=request.POST['first_name']
        last_name=request.POST['last_name']
        password = request.POST['password']
        user = User.objects.create_user(username, email, password)
        user.first_name = first_name
        user.last_name = last_name
        user.save()
        return render(request, 'login.html')
    return render(request, "register.html")

def UserLogin(request):
    if request.method=="POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, "Successfully Logged In")
            return redirect("/home")
        else:
            return HttpResponse("User not Found")

    return render(request, "login.html")


def forum(request):
    profile = Profile.objects.all()
    if request.method=="POST":
        user = request.user
        image = request.user.profile.image
        content = request.POST.get('content','')
        post = Post(user1=user, post_content=content, image=image)
        post.save()
        alert = True
        return render(request, "chat.html", {'alert':alert})
    posts = Post.objects.all()
    return render(request, "chat.html", {'posts':posts})


def discussion(request, user):
    post = Post.objects.filter(id=user).first()
    replies = Reply.objects.filter(post=post)
    if request.method=="POST":
        user = request.user
        image = request.user.profile.image
        desc = request.POST.get('desc','')
        post_id =request.POST.get('post_id','')
        reply = Reply(user = user, reply_content = desc, post=post, image=image)
        reply.save()
        alert = True
        return render(request, "discussion.html", {'alert':alert})
    return render(request, "discussion.html", {'post':post, 'replies':replies})

class ChatView(generic.ListView):
    model = Post
    template_name = 'chat.html'


class PostCreate(generic.CreateView):
    model = Post
    fields = ('post_content', 'timestamp')
    template_name = 'create.html'
    success_url = reverse_lazy('chat')

class PostUpdate(UpdateView):

    model = Post
    fields = ('post_content','timestamp', 'user', 'post_id')
    template_name = 'update.html'
    success_url = "chat"

    def get_queryset(self):
        queryset=super().get_queryset()
        queryset=queryset.filter(user=self.request.user)
        return queryset



class PostDelete(DeleteView):
    model = Post
    success_url = 'chat.html'
    template_name = 'delete.html'
